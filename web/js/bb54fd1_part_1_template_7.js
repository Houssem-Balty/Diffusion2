

var $collectionHolder;
var $collectionHolder2;

// setup an "add a tag" link
var $addTagLink = $('<a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Add variables </a> <br>');
var $newLinkLi = $('<span> </span>').append($addTagLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionHolder = $('td.values');
    $collectionHolder2 = $('td.variables');
    //console.log($collectionHolder) ;
    // add the "add a tag" anchor and li to the tags
    $(".addTag").append($newLinkLi);


    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $collectionHolder2.data('index', $collectionHolder2.find(':input').length);

    $addTagLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionHolder2, $newLinkLi );
        addTagForm($collectionHolder, $newLinkLi);

    });
});




function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');
    console.log(prototype);
    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<tr></tr>').append(newForm);
    $newLinkLi.before($newFormLi);
}
