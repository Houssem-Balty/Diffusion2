<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Application;
use AppBundle\Entity\Mail;
use AppBundle\Form\ApplicationType;
use AppBundle\Form\MailingListType;
use AppBundle\Form\MailType;

class MailController extends Controller
{
    /**
     * @Route("/mailing/list", name="mail_list")
     */

 public function listAction(Request $request)
    {


          return $this->render('application/application.html.twig', array('applications' => $applications));

    }


    /**
     * @Route("/mail/add", name="mail_add")
     */

 public function addAction(Request $request)
    {
      // Création d'un objet MailingList
      $mail = new Mail();

       // Génération du formulaire de login !
       $form = $this->createForm(MailType::class,$mail);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->persist($data);
             $em->flush();
           }

      return $this->render('/mail/mailAdd.html.twig',array(
       'form' => $form->createView(),
       ));


    }




}
