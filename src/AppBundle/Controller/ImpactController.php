<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Impact;
use AppBundle\Entity\Client;
use Doctrine\ORM\Query\Expr;
use AppBundle\Form\MetierType;
use AppBundle\Form\ImpactType;

class ImpactController extends Controller
{
    /**
     * @Route("/impact/list", name="impact_list")
     */

  public function listAction(Request $request)
    {

        $qb = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Impact')
       ->createQueryBuilder('c');
			 
			 $qb->select(array('u'))
									->from('AppBundle:Impact', 'u')
									->where('u.importance = :importance')
									->setParameter('importance',0);
				
				$impacts = $qb->getQuery()->getResult();
				

									
    	 return $this->render("impact/impact.html.twig",array(
       'impacts' => $impacts,
      ));
    }
									


		 /**
     * @Route("/impact/details/{idImpact}", name="impact_details")
     */

  public function detailsAction(Request $request, $idImpact)
    {

      $impact = $this
			->getDoctrine()
			->getRepository('AppBundle:Impact')
			->findOneById($idImpact);

    	return $this->render("impact/impactDetails.html.twig",array(
        'impact' => $impact,
      ));
    }


    /**
     * @Route("/impact/add", name="impact_add")
     */

  public function addAction(Request $request)
    {
			
			 $qb = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Impact')
       ->createQueryBuilder('c');
			 
			 $qb->select(array('u'))
									->from('AppBundle:Impact', 'u')
									->where('u.importance = :importance')
									->setParameter('importance',0);
				
				$impacts = $qb->getQuery()->getResult();
				
			
					// préparation du formulaire 
         $impact = new Impact();
         $form = $this->createForm(ImpactType::class,$impact);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()){
						$impact = $form->getData();
						$impact->setImportance(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($impact) ;
            $em->flush();
						
                   return $this->render('/impact/impactAdd.html.twig',array(
                     'form' => $form->createView(),
                     'impacts' => $impacts,
                     'success' => "Impact Ajouté avec succès!",
               ));						
						
						
         } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/impact/impactAdd.html.twig',array(
                     'form' => $form->createView(),
                     'impacts' => $impacts,
                     'errors' => $errors,
               ));

           }

         return $this->render("impact/impactAdd.html.twig",array(
           'form' => $form->createView(),
					 'impacts' => $impacts,
         ));

    }


        /**
         * @Route("/impact/update/{idImpact}", name="impact_update")
         */

      public function updateAction(Request $request, $idImpact)
        {
         $impact = $this->getDoctrine()
         ->getRepository("AppBundle:Impact")
         ->findOneById($idImpact) ;
         
         $form = $this->createForm(ImpactType::class,$impact);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()){
						$impact = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            
            return $this->redirect('/impact/list');

         } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/impact/impactUpdate.html.twig',array(
                     'form' => $form->createView(),
                     'errors' => $errors,
               ));

           }

         return $this->render("impact/impactUpdate.html.twig",array(
           'form' => $form->createView(),
         ));
        
        }
        
         /**
         * @Route("/metier/delete/{idImpact}", name="impact_delete")
         */

      public function deleteAction(Request $request, $idImpact)
        {
            $em = $this->getDoctrine()->getManager();
            $impact = $this->getDoctrine()->getRepository("AppBundle:Impact")->findOneById($idImpact) ;
            $em->remove($impact) ;
            $em->flush();
            return $this->redirect('/impact/list') ;
        }
        
        /**
         * @Route("/metier/activate/{idImpact}", name="impact_activate")
         */

      public function activateAction(Request $request, $idImpact)
        {
           $impact = $this->getDoctrine()
           ->getRepository("AppBundle:Impact")
           ->findOneById($idImpact);
           
           if ($impact->getActivationStatus() == 0){
               $impact->setActivationStatus(1) ; 
           } else {
            $impact->setActivationStatus(0);
           }
           
           $em = $this->getDoctrine()
           ->getManager();
           
           $em->flush($impact);
           return $this->redirect('/impact/list') ;
        }
}
