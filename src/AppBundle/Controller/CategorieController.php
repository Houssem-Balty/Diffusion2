<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Application;
use AppBundle\Entity\Categorie;
use AppBundle\Form\CategorieType;

class CategorieController extends Controller
{
    /**
     * @Route("/categorie/list", name="categorie_list")
     */

 public function listAction(Request $request)
    {

      $categories = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Categorie')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();





          return $this->render('categorie/categorie.html.twig', array('categories' => $categories));

    }


    /**
     * @Route("/categorie/add", name="categorie_add")
     */

 public function addAction(Request $request)
    {
        
         $categories = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Categorie')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

        

       $categorie = new Categorie();

       // Génération du formulaire de login !
       $form = $this->createForm(CategorieType::class,$categorie);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $data->setActivationStatus(true);
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->persist($data);
             $em->flush();
             
             return $this->redirect('/categorie/add');
           }
            else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/categorie/categorieAdd.html.twig',array(
                     'form' => $form->createView(),
                     'categories' => $categories,
                     'errors' => $errors,
               ));

           }

      return $this->render('/categorie/categorieAdd.html.twig',array(
       'form' => $form->createView(),
       'categories' => $categories,
       ));


    }

    /**
     * @Route("/categorie/deactivate/{id}", name="categorie_deactivate")
     */

  public function deactivateAction($id)
    {
         $categorie = $this->getDoctrine()
         ->getRepository("AppBundle:Categorie")
         ->findOneById($id) ;
         if ($categorie->getActivationStatus() == 0 )
              $categorie->setActivationStatus(1) ;
            else {
              $categorie->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;
            
            return $this->redirect('/categorie/add') ;
    }


    /**
     * @Route("/categorie/delete/{id}", name="categorie_delete")
     */

  public function deleteAction($id)
    {
         $categorie = $this->getDoctrine()
         ->getRepository("AppBundle:Categorie")
         ->findOneById($id) ;
         
            $em = $this->getDoctrine()->getManager();
            $em->remove($categorie);
            $em->flush() ;
            
            return $this->redirect('/categorie/add') ;
    }

    

    /**
     * @Route("/categorie/update/{idCategorie}", name="categorie_update")
     */

 public function updateAction(Request $request, $idCategorie)
    {
      // Récupération du l'application
      $categorie = $this->getDoctrine()
      ->getRepository('AppBundle:Categorie')
      ->findOneById($idCategorie);

      // Création du formulaire
      $form = $this->createForm(CategorieType::class, $categorie) ;

      // handling request

      $form->handleRequest($request) ;

      if($form->isValid() && $form->isSubmitted()){
        $categorie = $form->getData();
        // entity manager

        $em = $this->getDoctrine()->getEntityManager();
        $em->flush();
      } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/categorie/categorieUpdate.html.twig',array(
                     'form' => $form->createView(),
                     'errors' => $errors,
               ));

           }

      return $this->render('categorie/categorieUpdate.html.twig', array(
        'form' => $form->createView(),
      )) ;


    }



}
