<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Mail;
/**
 * MailingList
 *
 * @ORM\Table(name="mailing_list")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MailingListRepository")
 */
class MailingList
{


  public function __construct(){
     $this->mailCC = new \Doctrine\Common\Collections\ArrayCollection();
  }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    
    private $idMetier;




    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="impactLevel", type="integer")
     */
    private $impactLevel;
    
    


    
    private $mailCC;

    /**
     * @var string
     *
     * @ORM\Column(name="mailtTo", type="string")
     */
    
    private $mailTo ;

    /**
     * @var string
     *
     * @ORM\Column(name="mailingListName", type="string")
     */
    private $mailingListName;



        /**
         * Get mailingListName
         *
         * @return string
         */
        public function getMailingListName()
        {
            return $this->mailingListName;
        }


        /**
         * Set mailingListName
         *
         * @param string $mailingListName
         *
         * @return MailingList
         */
        public function setMailingListName($mailingListName)
        {
            $this->mailingListName = $mailingListName;

            return $this;
        }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMetier
     *
     * @param integer $idMetier
     *
     * @return MailingList
     */
    public function setIdMetier($idMetier)
    {
        $this->idMetier = $idMetier;

        return $this;
    }

    /**
     * Get idMetier
     *
     * @return int
     */
    public function getIdMetier()
    {
        return $this->idMetier;
    }

    /**
     * Set Client
     *
     * @param Client $client
     *
     * @return MailingList
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get Client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set impactLevel
     *
     * @param integer $impactLevel
     *
     * @return MailingList
     */
    public function setImpactLevel($impactLevel)
    {
        $this->impactLevel = $impactLevel;

        return $this;
    }

    /**
     * Get impactLevel
     *
     * @return int
     */
    public function getImpactLevel()
    {
        return $this->impactLevel;
    }

    /**
     * Set mailTo
     *
     * @param  $mailTo
     *
     * @return MailingList
     */
    public function setMailTo($mailTo)
    {
        $this->mailTo = $mailTo;

        return $this;
    }

    /**
     * Get mailTo
     *
     * @return String
     */
    public function getMailTo()
    {
        return $this->mailTo;
    }

    /**
     * Set mailCC
     *
     * @param array $mailCC
     *
     * @return MailingList
     */
    public function setMailCC($mailCC)
    {
        $this->mailCC = $mailCC;

        return $this;
    }

    /**
     * Get mailCC
     *
     * @return array
     */
    public function getMailCC()
    {
        return $this->mailCC;
    }
}
