<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Incident
 *
 * @ORM\Table(name="incident")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IncidentRepository")
 */
class Incident
{

    public function __construct(){
      $this->pos = new ArrayCollection();
      $this->diffusions = new ArrayCollection();
      $this->impactMetier = new ArrayCollection();
      $this->impactApplication = new ArrayCollection();
      $this->dateDebut = new \DateTime();
      $this->dateFin = new \DateTime();

    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * One Incident has Many Emails.
     * @ORM\OneToMany(targetEntity="Diffusion", mappedBy="incident")
     */

    private $diffusions ;


    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var int
     *
     * @ORM\Column(name="Impact", type="integer")
     */
    private $impact;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateFin", type="datetime")
     */
    private $dateFin;


    /**
     * @var string
     *
     * @ORM\Column(name="Type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="descTechnique", type="string", length=800)
     */
    private $descTechnique;



    /**
    * @ORM\ManyToOne(targetEntity="Client")
    * @ORM\JoinColumn(nullable=false)
    */
    private $client;

    /**
    *@ORM\OneToMany(targetEntity="Pos", mappedBy="incident", cascade={"persist","remove"})
    *
    */
    private $pos ;


    /**
     * Many Incidents can have many Impacted Applications
     * @ORM\ManyToMany(targetEntity="Metier")
     * @ORM\JoinTable(name="incident_metier",
     *      joinColumns={@ORM\JoinColumn(name="incident_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="metier_id", referencedColumnName="id")}
     *      )
     */

    private $impactMetier;


    /**
     * Many Incidents can have many Impacted Applications
     * @ORM\ManyToMany(targetEntity="Application")
     * @ORM\JoinTable(name="incident_application",
     *      joinColumns={@ORM\JoinColumn(name="incident_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id")}
     *      )
     */
    private $impactApplication;



    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=800)
     */

    private $statut ;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="dureeResolution", type="string", length=800)
     */

    private $dureeResolution ;
    
    
    
    




    public function addMetierImpacte(Metier $metier){
        $this->impactMetier[] = $metier ;
        return $this ;
    }


     public function addApplicationImpactee(Application $application){
        $this->impactApplication[] = $application ;
        return $this ;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Incident
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }




    /**
     * Get impactMetier
     *
     * @return object
     */
    public function getImpactMetier()
    {
        return $this->impactMetier;
    }


    /**
     * Get impactApplication
     *
     * @return object
     */
    public function getImpactApplication()
    {
        return $this->impactApplication;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }





    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Incident
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set impact
     *
     * @param integer $impact
     *
     * @return Incident
     */
    public function setImpact($impact)
    {
        $this->impact = $impact;

        return $this;
    }

    /**
     * Get impact
     *
     * @return int
     */
    public function getImpact()
    {
        return $this->impact;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Incident
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Incident
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Incident
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set descTechnique
     *
     * @param string $descTechnique
     *
     * @return Incident
     */
    public function setDescTechnique($descTechnique)
    {
        $this->descTechnique = $descTechnique;

        return $this;
    }

    /**
     * Get descTechnique
     *
     * @return string
     */
    public function getDescTechnique()
    {
        return $this->descTechnique;
    }

    /**
     * Set ressentiUtilisateur
     *
     * @param string $ressentiUtilisateur
     *
     * @return Incident
     */
    public function setRessentiUtilisateur($ressentiUtilisateur)
    {
        $this->ressentiUtilisateur = $ressentiUtilisateur;

        return $this;
    }

    /**
     * Get ressentiUtilisateur
     *
     * @return string
     */
    public function getRessentiUtilisateur()
    {
        return $this->ressentiUtilisateur;
    }

    /**
     * Set actionCorrective
     *
     * @param string $actionCorrective
     *
     * @return Incident
     */
    public function setActionCorrective($actionCorrective)
    {
        $this->actionCorrective = $actionCorrective;

        return $this;
    }

    /**
     * Get actionCorrective
     *
     * @return string
     */
    public function getActionCorrective()
    {
        return $this->actionCorrective;
    }

    /**
     * Set actionPreventive
     *
     * @param string $actionPreventive
     *
     * @return Incident
     */
    public function setActionPreventive($actionPreventive)
    {
        $this->actionPreventive = $actionPreventive;

        return $this;
    }

    /**
     * Get actionPreventive
     *
     * @return string
     */
    public function getActionPreventive()
    {
        return $this->actionPreventive;
    }

    /**
     * Set dateDebutImpact
     *
     * @param \DateTime $dateDebutImpact
     *
     * @return Incident
     */
    public function setDateDebutImpact($dateDebutImpact)
    {
        $this->dateDebutImpact = $dateDebutImpact;

        return $this;
    }

    /**
     * Get dateDebutImpact
     *
     * @return \DateTime
     */
    public function getDateDebutImpact()
    {
        return $this->dateDebutImpact;
    }

    /**
     * Set dateFinImpact
     *
     * @param \DateTime $dateFinImpact
     *
     * @return Incident
     */
    public function setDateFinImpact($dateFinImpact)
    {
        $this->dateFinImpact = $dateFinImpact;

        return $this;
    }

    /**
     * Get dateFinImpact
     *
     * @return \DateTime
     */
    public function getDateFinImpact()
    {
        return $this->dateFinImpact;
    }





    /**
     * Set Client
     *
     * @param integer $idClient
     *
     * @return Incident
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get Client
     *
     * @return int
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set dureeResolution
     *
     * @param string $dureeResolution
     *
     * @return Incident
     */
    public function setDureeResolution($dureeResolution)
    {
        $this->dureeResolution = $dureeResolution;

        return $this;
    }

    /**
     * Get dureeResolution
     *
     * @return string
     */
    public function getDureeResolution()
    {
        return $this->dureeResolution;
    }

    /**
     * Add diffusion
     *
     * @param \AppBundle\Entity\Diffusion $diffusion
     *
     * @return Incident
     */
    public function addDiffusion(\AppBundle\Entity\Diffusion $diffusion)
    {
        $this->diffusions[] = $diffusion;

        return $this;
    }

    /**
     * Remove diffusion
     *
     * @param \AppBundle\Entity\Diffusion $diffusion
     */
    public function removeDiffusion(\AppBundle\Entity\Diffusion $diffusion)
    {
        $this->diffusions->removeElement($diffusion);
    }

    /**
     * Get diffusions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiffusions()
    {
        return $this->diffusions;
    }

    /**
     * Add po
     *
     * @param \AppBundle\Entity\Pos $po
     *
     * @return Incident
     */
    public function addPo(\AppBundle\Entity\Pos $po)
    {
        $this->pos[] = $po;

        return $this;
    }

    /**
     * Remove po
     *
     * @param \AppBundle\Entity\Pos $po
     */
    public function removePo(\AppBundle\Entity\Pos $po)
    {
        $this->pos->removeElement($po);
    }

    /**
     * Get pos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Add impactMetier
     *
     * @param \AppBundle\Entity\Metier $impactMetier
     *
     * @return Incident
     */
    public function addImpactMetier(\AppBundle\Entity\Metier $impactMetier)
    {
        $this->impactMetier[] = $impactMetier;

        return $this;
    }

    /**
     * Remove impactMetier
     *
     * @param \AppBundle\Entity\Metier $impactMetier
     */
    public function removeImpactMetier(\AppBundle\Entity\Metier $impactMetier)
    {
        $this->impactMetier->removeElement($impactMetier);
    }

    /**
     * Add impactApplication
     *
     * @param \AppBundle\Entity\Application $impactApplication
     *
     * @return Incident
     */
    public function addImpactApplication(\AppBundle\Entity\Application $impactApplication)
    {
        $this->impactApplication[] = $impactApplication;

        return $this;
    }

    /**
     * Remove impactApplication
     *
     * @param \AppBundle\Entity\Application $impactApplication
     */
    public function removeImpactApplication(\AppBundle\Entity\Application $impactApplication)
    {
        $this->impactApplication->removeElement($impactApplication);
    }
}
